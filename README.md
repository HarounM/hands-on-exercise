# GitLab Hands-on Exercise

To complete the exercise, you should be acquainted with

 - [Git](https://try.github.io)
 - GitLab, especially the [Merge
   Requests](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html)
   feature
 - [GitLab CI](https://docs.gitlab.com/ce/ci/quick_start/README.html)

We don't require a specific programming language to be used. However, our base
repository contains code written in C and the corresponding compilation/testing
tools are employed.

## Exercise execution steps

You have to follow the following steps in order to complete the exercise.

### 1 Fork the central repository

While you're signed in, please just press the above `Fork` button.

### 2 Push three commits to your fork

The following three steps 2.1, 2.2, and 2.3 correspond to three separate commits
that you should make to pass the exercise. The commits should be initially
pushed to your personal repository (fork) created above.

#### 2.1 Add missing functional test

This repository contains the code for a simple calculator. It supports addition,
subtraction, and multiplication of two operands.

The initial test coverage is 91% as the current functional tests cover only the
addition and multiplication operations.

👉 Your first commit should contain a functional test for _subtraction_. This
will eventually increase the code coverage up to 100%!

#### 2.2 Add missing functionality

The original calculator does not support the _division_ operation between two
_integers_.

👉 Create a second commit that adds this functionality. In the same commit,
don't forget to test it to ensure that the test coverage remains 100%.

#### 2.3 Add memory test CI job

The current Continuous Integration (CI) pipeline begins with the `build` job and
continues with the `functional-test` job that belongs to the `test` stage.

👉 Create a third commit to run a new job named `memory-test` inside the `test`
stage too. Eventually, the new job will run in parallel with the existing
`functional-test` job, as they will belong to the same stage.

ℹ️ In order to do this, you will have to modify the `.gitlab-ci.yml` file. Add a
new job that executes e.g. `apt -y install valgrind` and
`valgrind --leak-check=full --error-exitcode=1 ./calculator 321 + 1078`
commands.

### 3 Create a Merge Request

Finally, you have to push your commits from your fork to the original central
(base) repository. This should be done via the Merge Request road. Please
mention the user(s) that you want to review your request, using the `@username`
syntax in the description.
